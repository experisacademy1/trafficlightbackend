﻿using Microsoft.Extensions.Caching.Memory;
using TrafficLight.Models;

namespace TrafficLight.Services
{
    public interface ITrafficLightService
    {

        public TrafficLightModel Start();

        public TrafficLightModel UpdateLight(int timer);

        public TrafficLightModel ExtendTime(int timer);
    }
}

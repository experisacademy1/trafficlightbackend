﻿using Microsoft.Extensions.Caching.Memory;
using TrafficLight.Configuration;
using TrafficLight.Exceptions;
using TrafficLight.Models;

namespace TrafficLight.Services
{
    public class TrafficLightService : ITrafficLightService
    {
        private readonly ITrafficLightService _service;
        private readonly IMemoryCache _cache;

        public TrafficLightService(ITrafficLightService service, IMemoryCache cache)
        {
            _service = service;
            _cache = cache;
        }

        public TrafficLightModel ExtendTime(int timer)
        {
            if (!_cache.TryGetValue("TrafficData", out TrafficLightModel trafficLight))
            {
                return null;
            }

            if (trafficLight.LightState == "Red")
            {
                throw new RedLightException("Can not push button while light is red!");
            }

            if (trafficLight.LightState == "Yellow" || trafficLight.LightState == "YellowGreen")
            {
                throw new YellowLightException("Can not push button while light is yellow!");
            }

            if(trafficLight.LightState == "YellowGreen") 
            { 
                throw new YellowGreenLightException("Can not push button while light is yellow and green!"); 
            }

            if (trafficLight.CurrentTimer + TrafficLightConfiguration.AddTime > TrafficLightConfiguration.MaxGreenTimer)
            {
                throw new MaxTimeException("Maximum time exceeded, can not extend it!");
            }

            trafficLight.CurrentTimer += TrafficLightConfiguration.AddTime;
            trafficLight.GreenTimer = timer + TrafficLightConfiguration.AddTime;
            return trafficLight;
        }

        public TrafficLightModel Start()
        {
            TrafficLightModel trafficLight = new TrafficLightModel() // See file TrafficLightConfiguration.cs to change values
            {
                GreenTimer = TrafficLightConfiguration.GreenTimer,
                RedTimer = TrafficLightConfiguration.RedTimer,
                LightState = TrafficLightState.TrafficState.Red.ToString(),
                CurrentTimer = TrafficLightConfiguration.GreenTimer
            };

            _cache.Set("TrafficData", trafficLight);
            Console.Write(trafficLight.ToString());
            return trafficLight;
        }

        public TrafficLightModel UpdateLight(int timer)
        {

            if (!_cache.TryGetValue("TrafficData", out TrafficLightModel trafficLight))
            {
                return null;
            }

            if (trafficLight.LightState == "Red")
            {
                if (timer == trafficLight.RedTimer)
                {
                    trafficLight.LightState = TrafficLightState.TrafficState.YellowGreen.ToString();
                }
            }

            // Reset timers in case button has been pushed
            else if (trafficLight.LightState == "Green")
            {
                trafficLight.LightState = TrafficLightState.TrafficState.Yellow.ToString();
                trafficLight.GreenTimer = TrafficLightConfiguration.GreenTimer;
                trafficLight.CurrentTimer = TrafficLightConfiguration.GreenTimer;
            }

            else if (trafficLight.LightState == "YellowGreen")
            {
                trafficLight.LightState = TrafficLightState.TrafficState.Green.ToString();
            }

            else if (trafficLight.LightState == "Yellow")
            {
                trafficLight.LightState = TrafficLightState.TrafficState.Red.ToString();
            }

            return trafficLight;
        }
    }
}

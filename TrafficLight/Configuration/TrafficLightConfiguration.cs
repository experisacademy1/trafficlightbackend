﻿namespace TrafficLight.Configuration
{
    public static class TrafficLightConfiguration
    {
        // The following values are used to change timers for the traffic light, feel free to change

        public const int RedTimer = 30;

        public const int GreenTimer = 30;

        public const int MaxGreenTimer = 360;

        public const int AddTime = 30;
    }
}

﻿using System.Text.Json.Serialization;
using System.Timers;

namespace TrafficLight.Models
{
    public class TrafficLightModel
    {
        public int? GreenTimer { get; set; }

        public int? RedTimer { get; set; }

        public int? CurrentTimer { get; set; }

        public string? LightState { get; set; }
    }
}

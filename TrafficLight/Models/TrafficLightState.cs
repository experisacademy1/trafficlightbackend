﻿namespace TrafficLight.Models
{
    public class TrafficLightState
    {
        public enum TrafficState
        {
            Red,
            Yellow,
            YellowGreen,
            Green
        }
    }
}

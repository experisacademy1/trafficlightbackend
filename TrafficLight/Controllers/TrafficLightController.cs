﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using TrafficLight.Configuration;
using TrafficLight.Exceptions;
using TrafficLight.Models;
using TrafficLight.Services;

namespace TrafficLight.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TrafficLightController : Controller
    {
        private readonly IMemoryCache _cache;
        private readonly ITrafficLightService _trafficLightService;
        public TrafficLightController(IMemoryCache cache)
        {
            _cache = cache;
            _trafficLightService = new TrafficLightService(_trafficLightService, _cache);
        }

        [HttpGet("/start")]
        public ActionResult<TrafficLightModel> Start()
        {
            var trafficLight = _trafficLightService.Start();

            return trafficLight;
        }

        [HttpGet("/view")]
        public ActionResult<TrafficLightModel> ViewTrafficLight()
        {
            if (_cache.TryGetValue("TrafficData", out TrafficLightModel trafficLight))
            {
                return trafficLight;
            }

            return NotFound();
        }

        [HttpPut("/setlight")]
        public ActionResult<TrafficLightModel> SetLight(int timer)
        {
            var trafficLight = _trafficLightService.UpdateLight(timer);

            if (trafficLight == null)
            {
                return NotFound();
            }

            return trafficLight;

        }

        [HttpPut("/pushbutton")]
        public ActionResult<TrafficLightModel> PressButton(int timer)
        {
            try
            {
                var trafficLight = _trafficLightService.ExtendTime(timer);

                if (trafficLight != null)
                {
                    return trafficLight;
                }
                return NotFound();

            }
            catch (RedLightException ex) 
            { 
                return BadRequest(ex.Message); 
            }

            catch (YellowGreenLightException ex) 
            {
                return BadRequest(ex.Message);
            }

            catch (YellowLightException ex) 
            { 
                return BadRequest(ex.Message); 
            }

            catch (MaxTimeException ex) 
            { 
                return BadRequest(ex.Message); 
            }

        }
    }
}

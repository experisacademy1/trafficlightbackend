﻿namespace TrafficLight.Exceptions
{
    public class YellowLightException : Exception
    {
        public YellowLightException() { }

        public YellowLightException(string message) : base(message) { }

        public YellowLightException(string message, Exception innerException) : base(message, innerException) { }
    }
}

﻿namespace TrafficLight.Exceptions
{
    public class RedLightException : Exception
    {
        public RedLightException() { }

        public RedLightException(string message) : base(message) { }

        public RedLightException(string message, Exception innerException) : base(message, innerException) { }
    }
}

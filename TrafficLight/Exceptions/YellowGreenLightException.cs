﻿namespace TrafficLight.Exceptions
{
    public class YellowGreenLightException : Exception
    {
        public YellowGreenLightException() { }

        public YellowGreenLightException(string message) : base(message) { }

        public YellowGreenLightException (string message, Exception innerException) : base(string.Format(message, innerException)) { }
       
    }
}

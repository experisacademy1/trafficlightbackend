﻿namespace TrafficLight.Exceptions
{
    public class MaxTimeException : Exception
    {
        public MaxTimeException() { }

        public MaxTimeException(string message) : base(message) { }

        public MaxTimeException(string message, Exception innerException) : base(message, innerException) { } 
    }
}

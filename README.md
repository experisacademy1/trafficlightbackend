# Trafficlight Backend

This project simulates a traffic light. This is the backend part of the application. You can find the frontend part at https://gitlab.com/experisacademy1/trafficlightapp

## Table of Contents

- [Clone](#clone)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Start](#start)
- [Contributors](#contributors)

## Clone

You can clone this repository for your own uses through the following command in your terminal.
```
$cd (Chosen folder)
$git clone https://gitlab.com/experisacademy1/trafficlightapp.git
```

## Install

This website runs on ASP.NET, as well as NSubstitute for unit-testing purposes. Use the NuGet package manager to install it as necessary.

## Usage

The goal with this application is to simulate a the backend portion of a traffic light. With the backend part you can edit some parts on how the frontend works through the file TrafficLightConfiguration.cs.

It also comes with some unit tests to "validate" functionality.

## Start

Simply run the project through visual studio and everything should set up on it's own.

## Contributors

Erik - @ermo1222

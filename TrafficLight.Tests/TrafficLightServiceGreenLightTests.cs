﻿using Microsoft.Extensions.Caching.Memory;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficLight.Configuration;
using TrafficLight.Exceptions;
using TrafficLight.Models;
using TrafficLight.Services;
using TrafficLight.Tests.Utilities;

namespace TrafficLight.Tests
{
    public class TrafficLightServiceGreenLightTests
    {
        private readonly ITrafficLightService _service = Substitute.For<ITrafficLightService>();
        private readonly IMemoryCache _cache = GreenLightMemoryCache.CreateCacheWithGreenLight();

        public TrafficLightServiceGreenLightTests()
        {
            _service = new TrafficLightService(_service, _cache);
        }

        [Fact]
        public void TrafficLight_PushButton_ShouldIncreaseTimer()
        {
            // Arrange
            int timer = TrafficLightConfiguration.GreenTimer;

            // Act
            var expected = TrafficLightConfiguration.GreenTimer + TrafficLightConfiguration.AddTime;
            var actual = _service.ExtendTime(timer);

            // Assert
            Assert.Equal(expected, actual.GreenTimer);
        }

        [Fact]
        public void TrafficLight_UpdateLight_ShouldReturnYellowLight()
        {
            // Arrange
            var timer = TrafficLightConfiguration.GreenTimer;
            var expected = TrafficLightState.TrafficState.Yellow.ToString();

            // Act
            var actual = _service.UpdateLight(timer);
            // Assert

            Assert.Equal(expected, actual.LightState);
        }
    }
}

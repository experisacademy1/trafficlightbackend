using Microsoft.Extensions.Caching.Memory;
using TrafficLight.Models;
using TrafficLight.Services;
using NSubstitute;
using TrafficLight.Configuration;
using TrafficLight.Exceptions;
using TrafficLight.Tests.Utilities;

namespace TrafficLight.Tests
{
    public class TrafficLightServiceRedLightTests
    {
        private readonly ITrafficLightService _service = Substitute.For<ITrafficLightService>();
        private readonly IMemoryCache _cache = RedLightMemoryCache.CreateCacheWithRedLight();

        public TrafficLightServiceRedLightTests()
        {
            _service = new TrafficLightService(_service, _cache);
        }


        [Fact]
        public void TrafficLight_Start_ShouldReturnTrafficLightStateAsRed()
        {
            // Arrange & Act
            var actual = _service.Start();

           // Assert
            Assert.Equal("Red", actual.LightState);
        }

        [Fact]
        public void TrafficLight_Start_ShouldReturnRedTimer()
        {
            // Arrange
            var expected = TrafficLightConfiguration.RedTimer;

            // Act
            var actual = _service.Start();

            // Assert
            Assert.Equal(expected, actual.RedTimer);
        }

        [Fact]
        public void TrafficLight_UpdateLight_ShouldReturnTrafficLightStateAsYellowGreen()
        {
            // Arrange
            int timer = TrafficLightConfiguration.RedTimer;
            var expected = TrafficLightState.TrafficState.YellowGreen.ToString();

            // Act
            var actual = _service.UpdateLight(timer);

            // Assert
            Assert.Equal(expected, actual.LightState);
        }

        [Fact]
        public void TrafficLight_PushButton_ShouldReturnErrorIfLightStateIsRed()
        {
            // Arrange
            int timer = TrafficLightConfiguration.RedTimer;

            // Act & Assert
            Assert.Throws<RedLightException>(() => _service.ExtendTime(timer));
        }

        
    }
}
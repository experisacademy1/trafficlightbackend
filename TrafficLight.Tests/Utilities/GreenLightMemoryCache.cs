﻿using Microsoft.Extensions.Caching.Memory;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficLight.Configuration;
using TrafficLight.Models;

namespace TrafficLight.Tests.Utilities
{
    internal class GreenLightMemoryCache
    {

        public static IMemoryCache CreateCacheWithGreenLight()
        {

            IMemoryCache _cache = Substitute.For<IMemoryCache>();

            _cache.TryGetValue("TrafficData", out TrafficLightModel trafficModel).Returns(x =>
            {
                x[1] = new TrafficLightModel()
                {
                    CurrentTimer = TrafficLightConfiguration.GreenTimer,
                    RedTimer = TrafficLightConfiguration.RedTimer,
                    GreenTimer = TrafficLightConfiguration.GreenTimer,
                    LightState = TrafficLightState.TrafficState.Green.ToString()
                };
                return true;
            });

            return _cache;
        }
    }
}

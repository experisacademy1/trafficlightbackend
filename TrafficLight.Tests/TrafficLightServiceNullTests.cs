﻿using Microsoft.Extensions.Caching.Memory;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficLight.Configuration;
using TrafficLight.Services;
using TrafficLight.Tests.Utilities;

namespace TrafficLight.Tests
{
    public class TrafficLightServiceNullTests
    {
        private readonly ITrafficLightService _service = Substitute.For<ITrafficLightService>();
        private readonly IMemoryCache _cache = Substitute.For<IMemoryCache>();

        public TrafficLightServiceNullTests()
        {
            _service = new TrafficLightService(_service, _cache);
        }

        [Fact]
        public void TrafficLight_ExtendTimeWithEmptycache_ShouldReturnNull()
        {
            // Arrange
            var timer = TrafficLightConfiguration.AddTime;

            // Act
            var actual = _service.ExtendTime(timer);

            // Assert
            Assert.Null(actual);
        }

        [Fact]
        public void TrafficLight_UpdateLightWithEmptyCache_ShouldReturnNull()
        {
            // Arrange
            var timer = TrafficLightConfiguration.AddTime;

            // Act
            var actual = _service.UpdateLight(timer);

            // Assert
            Assert.Null(actual);
        }
    }
}
